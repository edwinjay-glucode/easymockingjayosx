//
//  ViewController.m
//  EasyMockingJay
//
//  Created by Edwin Jay on 2017/02/09.
//  Copyright © 2017 Edwin Jay. All rights reserved.
//

#import "ViewController.h"



#include <arpa/inet.h>
#include <sys/socket.h>


@implementation ViewController  {
    
    NSArray *Ip;
    NSMutableArray *validIPArray;
    
    NSInteger port;
    
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
  
    validIPArray = [[NSMutableArray alloc]init];
    Ip = [[NSHost currentHost] addresses];
    
    for (NSString *item in Ip) {
        
        
        BOOL check =  [self checkIPAdress:item];
        
        if (check) [validIPArray addObject:item];
        
    }
    
    
    NSLog(@"%@",validIPArray);
    [self.ipComboBox removeAllItems];
    
    for (int i = 0; i < validIPArray.count; i++) {
        [self.ipComboBox addItemWithObjectValue:validIPArray[i]];
        
    }
    
    
    
    
    // Do any additional setup after loading the view.
}





-(BOOL)checkIPAdress:(NSString*)IPAdress {
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
                                  options:0
                                  error:nil];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:IPAdress options:0 range:NSMakeRange(0, [IPAdress length])];
    return (numberOfMatches==1?YES:NO);}









- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    
    // Update the view, if already loaded.
}






- (IBAction)onGetIP:(id)sender {
    // self.labelIP.cell.title = [self.ipComboBox stringValue];
}








- (IBAction)onStartMockService:(id)sender {
    
    
    // Start Broadcast
    [self startBroadcast];
    [self openWireMock];
    self.labelStatus.cell.title = @"WireMock is running";
    
    
}

- (IBAction)onCancelService:(id)sender {
    [self.service stop];
    self.labelStatus.cell.title = @"WireMock is not running";
    
}



- (void)startBroadcast {
    // Initialize GCDAsyncSocket
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    // Start Listening for Incoming Connections
    NSError *error = nil;
    if ([self.socket acceptOnPort:0 error:&error]) {
        // Initialize Service
        self.service = [[NSNetService alloc] initWithDomain:@"local." type:@"_easymockingjay._tcp." name:[NSString stringWithFormat:@"%@",[self.ipComboBox stringValue]] port:[self.socket localPort]];
        
        // Configure Service
        [self.service setDelegate:self];
        
        // Publish Service
        [self.service publish];
        
    } else {
        NSLog(@"Unable to create socket. Error %@ with user info %@.", error, [error userInfo]);
    }
}


- (void)netServiceDidPublish:(NSNetService *)service {
    NSLog(@"Bonjour Service Published: domain(%@) type(%@) name(%@) port(%i)", [service domain], [service type], [service name], (int)[service port]);
    port = [service port];
}

- (void)netService:(NSNetService *)service didNotPublish:(NSDictionary *)errorDict {
    NSLog(@"Failed to Publish Service: domain(%@) type(%@) name(%@) - %@", [service domain], [service type], [service name], errorDict);
}

- (void)socket:(GCDAsyncSocket *)socket didAcceptNewSocket:(GCDAsyncSocket *)newSocket {
    NSLog(@"Accepted New Socket from %@:%hu", [newSocket connectedHost], [newSocket connectedPort]);
    
    // Socket
    [self setSocket:newSocket];
    
    // Read Data from Socket
    [newSocket readDataToLength:sizeof(uint64_t) withTimeout:-1.0 tag:0];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)socket withError:(NSError *)error {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if (self.socket == socket) {
        [self.socket setDelegate:nil];
        [self setSocket:nil];
    }
}


-(void)openWireMock{
   // dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSTask *task;
    task = [[NSTask alloc]init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"wiremock" ofType:@"jar" inDirectory:@"wiremock"];
    task.launchPath = @"/usr/bin/java";
    NSLog(@"%@",path);
    task.arguments = @[@"-jar",path];
    NSLog(@"%@",path);

    
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];

    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    
    NSData *data;
    
    
    data = [file readDataToEndOfFile];
        
    
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog (@"script returned:WireMock is Running\n%@", string);
    
 //   });

}










@end
