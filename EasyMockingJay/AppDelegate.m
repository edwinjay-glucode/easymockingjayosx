//
//  AppDelegate.m
//  EasyMockingJay
//
//  Created by Edwin Jay on 2017/02/09.
//  Copyright © 2017 Edwin Jay. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
