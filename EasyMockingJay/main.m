//
//  main.m
//  EasyMockingJay
//
//  Created by Edwin Jay on 2017/02/09.
//  Copyright © 2017 Edwin Jay. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
