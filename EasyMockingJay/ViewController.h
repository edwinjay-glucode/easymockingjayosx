//
//  ViewController.h
//  EasyMockingJay
//
//  Created by Edwin Jay on 2017/02/09.
//  Copyright © 2017 Edwin Jay. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GCDAsyncSocket.h"

@interface ViewController : NSViewController <NSNetServiceDelegate, GCDAsyncSocketDelegate>


@property (weak) IBOutlet NSComboBox *ipComboBox;
@property (nonatomic, strong) NSNetService *service;
@property (nonatomic, strong) GCDAsyncSocket *socket;
@property (weak) IBOutlet NSTextField *labelStatus;


- (IBAction)onStartMockService:(id)sender;

- (IBAction)onCancelService:(id)sender;
@end

