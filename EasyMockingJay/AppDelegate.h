//
//  AppDelegate.h
//  EasyMockingJay
//
//  Created by Edwin Jay on 2017/02/09.
//  Copyright © 2017 Edwin Jay. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

